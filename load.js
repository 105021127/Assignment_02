var loadState = {
    preload: function(){
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2,150,'loading...',{font: '30px Arial', fill: '#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);

        // Display the progress bar
        var progressBar = game.add.sprite(game.width /2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progressBar);

        music_on = false;

        // Load all game assets
        game.load.spritesheet('player','assets/planes1.png',90.7,107);//如果要做jucify要改成spritesheet(19,35)
        game.load.image('helper','assets/helper.png');
        game.load.image('bullet','assets/bullet1.png');
        game.load.image('rocket','assets/rocket.png');
        game.load.image('little_enemy','assets/little_enemy1.png');
        game.load.image('brown_enemy','assets/brown_enemy.png');
        game.load.image('coin','assets/Coin.png');
        game.load.spritesheet('explosion', 'assets/explosion.png', 64, 64);
        game.load.image('background1', 'assets/background2.jpg');
        game.load.image('background', 'assets/background4.jpg');
        game.load.audio('playerfire','assets/player-fire.wav');
        game.load.audio('enemyfire','assets/enemy-fire.wav');
        game.load.audio('playerexplode','assets/explosion.wav');
        game.load.audio('enemyexplode','assets/player-explosion.wav');
        game.load.image('sound','assets/sound1.png');
        game.load.image('nosound','assets/nosound1.png');
        game.load.spritesheet('bin','assets/bin1.png', 158.5, 112.5);
    },
    create: function(){
        // Go to the menu state
        game.state.start('menu');
    }
};