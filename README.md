##Complete game process 15% [Done]
–start menu = => game view => game over => quit or play again

##Basic rules 20% [Done]
–Follow the basic rules of Raiden
    •player : can move, shoot and it’s life will decrease when touch enemy’s bullets
    (按上下左右可以控制藍色飛機移動，按下空白鍵可以讓藍色飛機射擊; 按下"WSAD"分別可以控制黃色飛機的上下左右，按下T鍵可以射擊)
    •Enemy : system should generate enemy and each enemy can move and attack
    •Map : background will move through the game

##Jucify mechanisms 15% [Done]
–Level : level will become harder or have different levels to play (state "simple" level and "advance" level)
–Skill : player can use ultimate skills to attack enemy (create player2 to help player1 to win the game) 

##Animations 10% [Done]
–Add animation to player (藍色飛機有噴火的動畫效果)

##Particle Systems 10% [Done]
–Add particle systems to player’s and enemy’s bullets (在被撞到或是被射到時會有爆炸動畫)

##Sound effects 5% [Done]
–At least two kinds of sound effect (射擊時會發出音效/ 敵機被炸到時有爆炸音效 /我機在被碰撞或被射到會有爆炸音效 /背景音樂)
–Can change volume (menu 右上角有個喇叭，按下去可以變靜音)
–Ex : shoot sound effect, bgm

##UI 5% [Done] -- Game pause
–Player health, Ultimate skill number or power,Score, volume control and Game pause

##Leaderboard : 5%
–Store player's name and score in firebase real time database, and design a way to show a leaderboard to your game

##Appearance (subjective ) : 5%

•
Bonus :
–
Multi player game
•
On line : 15%
•
##Off line : 5% [Done] (有player2)
–
Enhanced items : Player get the item then it can
become more powerful.
•
Bullet automatic aiming : 5%
•
Unique bullet : 5%
•
##Little helper : 5% [Done] (player2 可擔任 player1的helper)
–
Boss
•
Unique movement and attack mode : 5%
–
Others?
## 1.紀錄最高分