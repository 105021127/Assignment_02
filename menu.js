var soundcontrol = 0;

var menuState = {
    preload: function(){
        game.load.audio('backgroundmusic','assets/Fantasy_Game_Background.mp3');
        //game.load.image('nosound', 'assets/nosound.png');
    },
    create: function() {
        // Add a background image
        game.add.image(0, 0, 'background1');

        // Add a background music
        
        //this.backgroundmusic.allowMultiple = true;
        //this.backgroundmusic.addMarker('backgroundmusic');
        this.backgroundmusic = game.add.audio('backgroundmusic');
        if(music_on == false){
            this.backgroundmusic.loopFull(); 
            this.backgroundmusic.play();
            music_on = true;
        }

        //Display sound sign
        this.sound = game.add.sprite(1090,30,'sound');
        //this.sound.anchor.setTo(0.5,0.5);
        game.input.onDown.add(this.soundControl,this);

        // Display the name of the game
        var nameLabel = game.add.text(game.width/2,150,'Raiden',{font: '50px PixelMplus', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5,0.5);

        // Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width/2,game.height-100,'score: '+ game.global.score, {font: '25px PixelMplus', fill: '#ffffff'});
        scoreLabel.anchor.setTo(0.5,0.5);

        // Explain how to start the game
        var startLabel = game.add.text(game.width/2,game.height-390,'SIMPLE',{font: '25px PixelMplus', fill:'#ffffff'});
        startLabel.anchor.setTo(0.5,0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'

        
        
        startLabel.inputEnabled = true;
        startLabel.events.onInputUp.add(function(){
            startlevel=0;
            //this.start();
            game.state.start('play');
        });

        this.AdvanceLabel = game.add.text(game.width/2,game.height-310,'ADVANCE',{font: '25px PixelMplus', fill:'#ffffff'});
        
        this.AdvanceLabel.anchor.setTo(0.5,0.5);
        this.AdvanceLabel.inputEnabled = true;
        
        this.AdvanceLabel.events.onInputUp.add(function(){
            startlevel=1;
            //this.start;
            game.state.start('play');
        });

        this.About = game.add.text(game.width/2,game.height-230,'ABOUT',{font: '25px PixelMplus', fill:'#ffffff'});
        this.About.anchor.setTo(0.5,0.5);
        this.About.inputEnabled = true;

        this.AdvanceLabel.events.onInputUp.add(function(){
            startlevel=1;
            //this.start;
            game.state.start('play');
        });

        //var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        //upKey.onDown.add(this.start,this);
       
    },
    
    soundControl: function(pointer){
        if(pointer.y<120 && pointer.y>20 && pointer.x<1170 && pointer.x>1070 && soundcontrol==0){
            this.backgroundmusic.pause();
            this.sound = game.add.sprite(1090,30,'nosound');
            soundcontrol=1;
        }
        else if(pointer.y<120 && pointer.y>20 && pointer.x<1170 && pointer.x>1070 && soundcontrol==1){
            this.sound = game.add.sprite(1090,30,'sound');
            this.backgroundmusic.resume();
            soundcontrol=0;
        }
    },
    
    /*start: function(){
        // Start the actual game
        if(startlevel==0){
            game.state.start('main');
        }
        else if(startlevel==1){
            game.state.start('play');
        }
    },*/

};