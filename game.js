// Initialize Phaser
var game = new Phaser.Game(1200,650,Phaser.AUTO,'canvas');
// Define our global variable
game.global = { score: 0 };
var startlevel;
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('about',aboutState);
game.state.add('play',playState);
// Start the 'boot' state
game.state.start('boot');