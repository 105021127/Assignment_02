var playState = {
    preload: function(){
        
    },
    create: function(){
        game.stage.backgroundColor= '#3498db';
        tileSprite = game.add.tileSprite(0,0,1200,650,'background');

        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        //code for the pause menu
        this.pause_label = game.add.text(1090,30,'Pause',{ font:'24px PixelMplus', fill:'#fff'});
        this.pause_label.inputEnabled = true;
        this.pause_label.events.onInputUp.add(function(){
            // When the paus button is pressed, we pause the game
            game.paused = true;

            choiseLabel = game.add.text(game.width/2, game.height/2, 'Click anywhere to continue', { font: '30px Arial', fill: '#fff' });
            choiseLabel.anchor.setTo(0.5, 0.5);
        });
        game.input.onDown.add(unpause, self);

        //unpaused game
        function unpause(event){
        if(game.paused){
            choiseLabel.destroy();
            game.paused = false;
        }
    }
        
        //display player on the screen with 'game.add.sprite'.
        this.player = game.add.sprite(game.width/2,game.height*3/4,'player');
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5,0);
        this.player.animations.add('play',[0,1,2,3],15,true);
        this.player.body.collideWorldBounds = true;//玩家會碰到邊邊

        //display bin
        /*this.bin = game.add.sprite(game.width/2,game.height*1/5,'bin');
        game.physics.arcade.enable(this.bin);
        this.bin.anchor.setTo(0.5,0.5);
        this.bin.animations.add('bindance',[0,1,2,3],15,true);
        this.bin.body.collideWorldBounds = true;//玩家會碰到邊邊*/

        
        //display helper
        this.helper = game.add.sprite(game.width*37/100,game.height*4/5,'helper');
        game.physics.arcade.enable(this.helper);
        this.helper.anchor.setTo(0.5,0);
        this.helper.body.collideWorldBounds = true;

        //display bullet
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(1000,'bullet');//一次產生100發子彈
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 0.5);

        this.nextShotAt = 0;//下一個子彈可以發射的時間(會與現在時間比較，因此設定為 0 就可以立即發射)
        this.shotDelay = 100;//發射子彈之間的延遲為 0.1 秒鐘 (亦即 0.1 秒鐘可以發射一發子彈)
        this.helpernextShot = 0;
        this.helpershotDelay = 100; 

        //display rocket(enemy bullet)
        this.rockets = game.add.group();
        this.rockets.enableBody = true;
        this.rockets.createMultiple(1000,'rocket');
        this.rockets.setAll('anchor.x', 0.5);
        this.rockets.setAll('anchor.y', 0.5);

        this.nextRocketAt = 0;//下一個子彈可以發射的時間(會與現在時間比較，因此設定為 0 就可以立即發射)
        this.Rocketdelay = 100;//發射子彈之間的延遲為 0.1 秒鐘 (亦即 0.1 秒鐘可以發射一發子彈)

        

        //display little_enemy
        this.little_enemy = game.add.group();
        this.little_enemy.enableBody = true;// Add Arcade physics to the whole group
        this.little_enemy.createMultiple(200,'little_enemy');//一次產生200台敵機
        this.little_enemy.setAll('anchor.x',0.5);
        this.little_enemy.setAll('anchor.y',0.5);
        this.little_enemy.setAll('outOfBoundskill',true);//所有成員離開世界邊界就會被殺死
        this.little_enemy.setAll('checkWorldBounds',true);//所有成員都需判斷是否在世界邊界
        

        this.nextEnemyAt = 0;//下一個敵機出發的時間(會與現在時間比較，因此設定為 0 就會立即出現)
        this.enemyDelay = 1000;//兩架敵機先後出現之間的延遲為 1 秒鐘 (亦即 1 秒鐘出現一架敵機)

        //tell Phaser which keys we want to use in our game.
        this.cursor = game.input.keyboard.createCursorKeys();
        this.W = game.input.keyboard.addKey(Phaser.Keyboard.W);//上
        this.A = game.input.keyboard.addKey(Phaser.Keyboard.A);//右
        this.S = game.input.keyboard.addKey(Phaser.Keyboard.S);//下
        this.D = game.input.keyboard.addKey(Phaser.Keyboard.D);//左
        
        //Display Player1 Life
        this.ultLabel = game.add.text(30,70,'Player1: Press SpaceBar to shoot',{font: '18px Arial', fill:'#ffffff'});
        //this.ultcount = 3;
        this.life2Label = game.add.text(30,100,'Player2: Press "T" to shoot',{font: '18px Arial', fill:'#ffffff'});
        //this.life2 = 5;

        //Display Score
        this.scoreLabel = game.add.text(30,30,'score: 0',{font: '24px Arial', fill:'#ffffff'});
        this.score = 0;

        //Display sound
        this.sound_playerfire = game.add.audio('playerfire');
        this.sound_playerfire.allowMultiple = true;
        this.sound_playerfire.addMarker('playerfire');

        this.sound_enemyfire = game.add.audio('enemyfire');
        this.sound_enemyfire.allowMultiple = true;
        this.sound_enemyfire.addMarker('enemyfire');

        this.sound_playerexplode = game.add.audio('playerexplode');
        this.sound_playerexplode.allowMultiple = true;
        this.sound_playerexplode.addMarker('playerexplode');

        this.sound_enemyexplode = game.add.audio('enemyexplode');
        this.sound_enemyexplode.allowMultiple = true;
        this.sound_enemyexplode.addMarker('enemyexplode');

        //display brown_enemy
        //this.brown_enemy = game.add.sprite(game.width/9,game.height*1/3,'brown_enemy');

        //diaplay coin
        /*this.coin = game.add.sprite(60,140,'coin');
        game.physics.arcade.enable(this.coin);
        this.coin.anchor.setTo(0.5,0.5);*/
    },
    update: function(){
        //背景移動
        tileSprite.tilePosition.y += 2;
        //
        //this.bin.animations.play('bindance');
        
        //讓飛機噴火的特效
        this.player.animations.play('play');
        
        this.moveEnemy();
        this.movePlayer();
        this.moveHelper();
        this.fire();
        //this.enemyfire();
        this.helpfire();

        //Enemy & bullet collapes
        game.physics.arcade.overlap(this.bullets,this.little_enemy,this.BulletHitEnemy,null,this);
        
        //Player hit enemy
        game.physics.arcade.overlap(this.player,this.little_enemy,this.PlayerHitEnemy,null,this);

        //Enemy bullet hit player
        game.physics.arcade.overlap(this.rockets,this.player,this.BulletHitPlayer,null,this);

        //Enemy bullet hit player bullet
        game.physics.arcade.overlap(this.bullets,this.rockets,this.BulletHitRocket,null,this);

        //Helper hit enemy
        game.physics.arcade.overlap(this.helper,this.little_enemy,this.HelperHitEnemy,null,this);

        //Enemy bullet hit helper(要改)
        game.physics.arcade.overlap(this.rockets,this.helper,this.BulletHitHelper,null,this);
 

        
        //Call it when player and coin overlap
        //game.physics.arcade.overlap(this.player,this.coin,this.takeCoin,null,this);

        //Call playerDie
        if(!this.player.inWorld){
            this.playerDie();
        }
    },

    movePlayer: function(){
        if(this.cursor.left.isDown){
            this.player.body.velocity.x = -300;
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 300;
        }
        else if(this.cursor.up.isDown){
            this.player.body.velocity.y = -300;
        }
        else if(this.cursor.down.isDown){
            this.player.body.velocity.y = 300;
        }
        else{
            //Stop the player
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
        }
    },//逗號很重要!!!

    moveHelper: function(){
        if(this.A.isDown){
            this.helper.body.velocity.x = -300;
        }
        else if(this.D.isDown){
            this.helper.body.velocity.x = 300;
        }
        else if(this.W.isDown){
            this.helper.body.velocity.y = -300;
        }
        else if(this.S.isDown){
            this.helper.body.velocity.y = 300;
        }
        else{
            //Stop the player
            this.helper.body.velocity.x = 0;
            this.helper.body.velocity.y = 0;
        }
    },

    //Enemy auto move & enemy fire
    moveEnemy: function(){
        if(this.nextEnemyAt<game.time.now && this.little_enemy.countDead()>0){
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.little_enemy.getFirstExists(false); //群組在成立時，成員預設值為死亡， 此函式參數若為 true 則取第一個活著的成員，否則取第一個死亡的成員
            enemy.reset(game.rnd.integerInRange(150, 1180), 0);
            enemy.body.velocity.y = game.rnd.integerInRange(30, 600);

            if(!enemy.alive || this.nextRocketAt>game.time.now){
                return;
            }
            if(this.rockets.countDead()==0){
                return;
            }
            this.nextRocketAt = game.time.now + this.Rocketdelay;

            var rocket = this.rockets.getFirstExists(false);
            
            if(startlevel==1){ //Advance 導彈型
                rocket.reset(enemy.x,enemy.y);
                rocket.rotation = this.game.physics.arcade.moveToObject(rocket, this.player, 500);
            }
            else if(startlevel==0){ //Simple 直線型
                rocket.reset(enemy.x,enemy.y-10);
                rocket.body.velocity.y = 1000;
            }
            
        }
    },

    //Restart game
    playerDie: function(){
        if(this.score>game.global.score){
            game.global.score=this.score; //在主選單顯示自己最高成績
        }
        game.state.start('menu');
    },

    //Enemy & bullet collapes
    BulletHitEnemy: function(bullets,little_enemy){
        this.takePoint();//加分!!!
        
        bullets.kill();
        little_enemy.kill();

        var explode = game.add.sprite(little_enemy.x,little_enemy.y,'explosion');
        explode.anchor.setTo(0.5);
        explode.animations.add('boom');
        explode.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除

        this.sound_enemyexplode.play('enemyexplode');
    },
    //Player hit enemy
    PlayerHitEnemy: function(player,little_enemy){
        little_enemy.kill();
        var explosion = game.add.sprite(player.x,player.y,'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除
        var explode = game.add.sprite(little_enemy.x,little_enemy.y,'explosion');
        explode.anchor.setTo(0.5);
        explode.animations.add('boom');
        explode.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除
        player.kill();

        if(!this.helper.alive){
            //這裡要加入遊戲重新開始
            game.time.events.add(1000,this.playerDie,this);//player不知道要不要括號!!!??????
        }

        this.sound_enemyexplode.play('enemyexplode');
        this.sound_playerexplode.play('playerexplode');
    },

    //Player hit enemy bullet
    BulletHitPlayer: function(rockets,player){
        rockets.kill();
        var explosion = game.add.sprite(player.x,player.y,'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除
        player.kill();

        if(!this.helper.alive){
            //這裡要加入遊戲重新開始
            game.time.events.add(1000,this.playerDie,this);//player不知道要不要括號!!!??????
        }
        
        this.sound_playerexplode.play('playerexplode');
    },

    //Bullet hit Rocket
    BulletHitRocket: function(bullets,rockets){
        bullets.kill();
        rockets.kill();

        var explode = game.add.sprite(rockets.x,rockets.y,'explosion');
        explode.anchor.setTo(0.5);
        explode.animations.add('boom');
        explode.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除

        this.sound_enemyexplode.play('enemyexplode');
    },

    //Helper Hit Enemy
    HelperHitEnemy: function(helper,little_enemy){
        little_enemy.kill();
        var explosion = game.add.sprite(helper.x,helper.y,'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除
        var explode = game.add.sprite(little_enemy.x,little_enemy.y,'explosion');
        explode.anchor.setTo(0.5);
        explode.animations.add('boom');
        explode.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除
        helper.kill();

        if(!this.player.alive){
            //這裡要加入遊戲重新開始
            game.time.events.add(1000,this.playerDie,this);//player不知道要不要括號!!!??????
        }

        this.sound_enemyexplode.play('enemyexplode');
        this.sound_playerexplode.play('playerexplode');
    },

    //Helper hit bullet
    BulletHitHelper: function(rockets,helper){
        rockets.kill();
        var explosion = game.add.sprite(helper.x,helper.y,'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom',15,false,true);//false ：不要重複播放， true ：播放後刪除
        helper.kill();

        if(!this.player.alive){
            //這裡要加入遊戲重新開始
            game.time.events.add(1000,this.playerDie,this);//player不知道要不要括號!!!??????
        }
        
        this.sound_playerexplode.play('playerexplode');
    },

    //emit bullet
    fire: function() { 
        if (!this.player.alive  || this.nextShotAt>game.time.now) {
          return;
        }
        if (this.bullets.countDead()==0) {
          return;
        }
        this.nextShotAt = game.time.now + this.shotDelay;
        
        var bullet = this.bullets.getFirstExists(false);
        var firebutton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        //var ultbutton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        //var temp = 0;
        //var ulttime;
        /*if(ultbutton.isDown){
            temp = 1;
            ulttime = this.game.time.now;
            this.ultcount--;
        }*/
        if(bullet && firebutton.isDown){
            this.sound_playerfire.play('playerfire');
            bullet.reset(this.player.x, this.player.y-30);
            
            //bullet.rotation = this.game.physics.arcade.moveToObject(bullet, this.little_enemy, 500);
            bullet.body.velocity.y = -500;
        }
        /*else if(bullet && temp==1 && this.ultcount>0 && this.game.time.now-ulttime<50000){ /////////////////////////////////////////////////
            
            this.ultLabel.text = 'Ultimate Skill(Press SpaceBar): '+this.ultcount;
            this.sound_playerfire.play('playerfire');
            bullet.reset(this.player.x, this.player.y-30);
            
            //bullet.rotation = this.game.physics.arcade.moveToObject(bullet, this.little_enemy, 500);
            bullet.body.velocity.y = -500;
        }*/
      },

    helpfire: function(){
        if (!this.helper.alive  || this.helpernextShot>game.time.now) {
            return;
          }
          if (this.bullets.countDead()==0) {
            return;
          }
          this.helpernextShot = game.time.now + this.helpershotDelay;
          
          var bullet = this.bullets.getFirstExists(false);
          var firebutton = game.input.keyboard.addKey(Phaser.Keyboard.T);
          if(bullet && firebutton.isDown){
              this.sound_playerfire.play('playerfire');
              bullet.reset(this.helper.x, this.helper.y-30);
              bullet.body.velocity.y = -500;
          }
      },

     /* //enemy emit bullet
      enemyfire: function(){
        if(this.game.time.now > this.nextRocketAt && this.rockets.countDead()>0){
            this.nextRocketAt = this.game.time.now + this.Rocketdelay;
            
            var rocket = this.rockets.getFirstExists(false);
            if(startlevel==1){
                rocket.reset(this.little_enemy.x,this.little_enemy.y);
                rocket.rotation = this.game.physics.arcade.moveToObject(rocket, this.player, 500);
            }
            else if(startlevel==0){
                rocket.reset(this.little_enemy.x,this.little_enemy.y);
                rocket.body.velocity.y = 500;
            }
        }
      },*/

      /*enemyfire2: function(enemy){
        
                
        var rocket = this.rockets.getFirstDead();

        rocket.reset(enemy.x,enemy.y);
        if(startlevel==1){
            rocket.rotation = this.game.physics.arcade.moveToObject(rocket, this.player, 500);
        }
        else if(startlevel==0){
            rocket.body.velocity.y = 500;
        }
      }, */

    


    //Get point when take coin
    takePoint: function(player,coin){
        
        this.score += 1;
        this.scoreLabel.text = 'score: '+this.score;
    },

    

};
